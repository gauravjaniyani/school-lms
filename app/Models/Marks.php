<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Marks extends Model {
  protected $table = 'Marks';
	protected $fillable = ['regiNo',
	'regiNo',
	'exam',
	'subject',
	'written',
	'mcq',
	'practical',
	'ca'
		];
		
		public function firstTerm(){
			return $this->hasOne("App\Models\Marks","regiNo",'regiNo')->where("exam","First Term");
	}
	
	public function midTerm(){
			return $this->hasOne("App\Models\Marks","regiNo",'regiNo')->where("exam","Mid Term");
	}
	public function subjectInfo(){
		return $this->hasOne("App\Models\Subject","code",'subject');
	}

}
