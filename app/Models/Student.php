<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Marks;
class Student extends Model {
	protected $table = 'Student';
	protected $fillable = ['regiNo',
	'firstName',
	'lastName',
	'middleName',
	'gender',
	'religion',
	'bloodgroup',
	'nationality',
	'dob',
	'session',
	'class',
	'photo',
	'fatherName',
	'fatherCellNo',
	'motherName',
	'motherCellNo',
	'presentAddress',
	'parmanentAddress',
	'general_register_number',
	'uid_number',
	'adhar_card_number',
  'admissiondate',
  'school_days',
  'present_days'
];

protected $primaryKey = 'id';
public function attendance(){
	$this->primaryKey = "regiNo";
	return $this->hasMany('App\Models\Attendance','regiNo');
}

public function firstTerm(){
    return $this->hasOne("App\Models\Marks","regiNo",'regiNo')->where("exam","First Term");
}

public function midTerm(){
    return $this->hasOne("App\Models\Marks","regiNo",'regiNo')->where("exam","Mid Term");
}

public function finalExam(){
    return $this->hasOne("App\Models\Marks","regiNo",'regiNo')->where("exam","Final Exam");
}

}
