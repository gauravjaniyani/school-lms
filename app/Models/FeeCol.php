<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class FeeCol extends Model {
	protected $table = 'stdBill';
	protected $fillable = ['billNo','class','regiNo','payableAmount','paidAmount','dueAmount','payDate'];
	public function student(){
        return $this->belongsTo('App\Models\Student','regiNo','regiNo');
	}
}
