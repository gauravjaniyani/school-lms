<?php
namespace App\Http\Controllers;
use App\Models\ClassModel;
use App\Models\Institute;
use App\Models\Subject;
use App\Models\GPA;
use App\Models\Marks;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Request;

class markController extends Controller {


	public function __construct()
    {
        $this->middleware('auth');

    }
	/**
	* Display a listing of the resource.
	*
	* @return Response
	*/
	public function index()
	{
		$classes = ClassModel::select('code','name')->orderby('code','asc')->get();
		$subjects = Subject::all();
		return View::Make('app.markCreate',compact('classes','subjects'));
	}


	/**
	* Show the form for creating a new resource.
	*
	* @return Response
	*/
    public function create(Request $request){
		$len = count(Input::get('regiNo'));

			$addRegNo = Input::get('addRegNo');
			$editRegNo = Input::get('editRegNo');

			$addfa1=Input::get('addfa1');
            $addfa2=Input::get('addfa2');
            $addsa1=Input::get('addsa1');

            $editfa1=Input::get('editfa1');
            $editfa2=Input::get('editfa2');
            $editsa1=Input::get('edotsa1');

      $addMarks = Input::get('addMarks');
			$editMarks = Input::get('editMarks');
			$addIsabsent = Input::get('addIsabsent');
			$editIsabsent = Input::get('editIsabsent');
			$addCounter=0;
			$notAddReg = array();
			if(is_array($addIsabsent)){
			foreach($addIsabsent as $index=>$attendance){
				$marks = new Marks;
				$marks->class=Input::get('class');
				$marks->section=Input::get('section');
				$marks->shift=Input::get('shift');
				$marks->session=trim(Input::get('session'));
				$marks->regiNo=$addRegNo[$index];
				$regNo = $addRegNo[$index];
				$marks->exam=Input::get('exam');
				$marks->subject=Input::get('subject');

				if(!empty($addfa1) || !empty($addfa2) || !empty($addsa1)){
                    $marks->fa1 = $addfa1[$regNo];
                    $marks->fa2 = $addfa2[$regNo];
                    $marks->sa1 = $addsa1[$regNo];
                }
				$marks->total= isset($addMarks[$regNo])?$addMarks[$regNo]:0;
				$subject = Subject::where('code',Input::get('subject'))->get()->first();
				$marks->total_subject_mark=$subject->totalfull;
				$marks->total_passing_mark=$subject->totalpass;
				$marks->exam_type=$subject->exam_type;

				$percentage = ($marks->total*100)/$subject->totalfull;
				if($percentage>=80) $grade = "A";
				if($percentage>=65 and $percentage<80) $grade = "B";
				if($percentage>=50 and $percentage<65) $grade = "C";
				if($percentage>=35 and $percentage<50) $grade = "D";
				if($percentage<35) $grade = "E" ;

				$marks->grade=$grade;
				$marks->point= 0;
				$marks->Absent=$attendance;
				if($marks->save()){
						$addCounter++;
				}else{
					$notAddReg[] = $addRegNo[$index];
				}
			}
		}
			$editCounter = 0;
			$noteditReg = array();
			if(is_array($editIsabsent)){
			foreach($editIsabsent as $id=>$attendance){
				$marks = Marks::find($id);
				$marks->class=Input::get('class');
				$marks->section=Input::get('section');
				$marks->shift=Input::get('shift');
				$marks->session=trim(Input::get('session'));
				$marks->regiNo = $editRegNo[$id];
				$marks->exam=Input::get('exam');
				$marks->subject=Input::get('subject');

				if(!empty($editfa1) || !empty($editfa2) || !empty($editsa1)){
                    $marks->fa1 = $editfa1[$id];
                    $marks->fa2 = $editfa2[$id];
                    $marks->sa1 = $editsa1[$id];
                }

				$marks->total= isset($editMarks[$id])?$editMarks[$id]:0;
				$marks->point= 0;
				$marks->Absent=$attendance;
				$subject = Subject::where('code',Input::get('subject'))->get()->first();
				$marks->total_subject_mark=$subject->totalfull;
				$marks->total_passing_mark=$subject->totalpass;
				$marks->exam_type=$subject->exam_type;

				$percentage = ($marks->total * 100 )/ $subject->totalfull;
				if($percentage>=80) $grade = "A";
				if($percentage>=65 and $percentage<80) $grade = "B";
				if($percentage>=50 and $percentage<65) $grade = "C";
				if($percentage>=35 and $percentage<50) $grade = "D";
				if($percentage<35) $grade = "E" ;

				$marks->grade=$grade;
				if($marks->save()){
						$editCounter++;
				}else{
					$noteditReg[] = $addRegNo[$index];
				}
			}
		}
			$counter = $addCounter + $editCounter;
			return Redirect::to('/mark/create')->with("success",$counter."'s student mark save Succesfully.");
	}



	/**
	* Display the specified resource.
	*
	* @param  int  $id
	* @return Response
	*/
	public function show(){
		$formdata = new formfoo;
		$formdata->class="";
		$formdata->section="";
		$formdata->shift="";
		$formdata->session="";
		$formdata->subject="";
		$formdata->exam="";
		$classes = ClassModel::select('code','name')->orderby('code','asc')->get();
		//$subjects = Subject::pluck('name','code');
		$marks=array();

		$subjects = Subject::all();
		//$formdata["class"]="";

		return View::Make('app.markList',compact('classes','marks','formdata','subjects'));
	}

	public function getlist()
	{

		$rules=[
			'class' => 'required',
			'section' => 'required',
			'shift' => 'required',
			'session' => 'required',
			'exam' => 'required',
			'subject' => 'required',

		];

		$validator = \Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{

			return Redirect::to('/mark/list/')->withErrors($validator);
		}
		else {
			$classes2 = ClassModel::orderby('code','asc')->pluck('name','code');
			$subjects = Subject::where('class',Input::get('class'))->pluck('name','code');
			$marks=	DB::table('Marks')
			->join('Student', 'Marks.regiNo', '=', 'Student.regiNo')
			->select('Marks.id','Marks.regiNo','Student.rollNo', 'Student.firstName','Student.middleName','Student.lastName', 'Marks.written','Marks.mcq','Marks.practical','Marks.ca','Marks.total','Marks.grade','Marks.point','Marks.Absent')
			->where('Student.isActive', '=', 'Yes')
			->where('Student.class','=',Input::get('class'))
			->where('Marks.class','=',Input::get('class'))
			->where('Marks.section','=',Input::get('section'))
			->Where('Marks.shift','=',Input::get('shift'))
			->where('Marks.session','=',trim(Input::get('session')))
			->where('Marks.subject','=',Input::get('subject'))
			->where('Marks.exam','=',Input::get('exam'))
			->get();

			$formdata = new formfoo;
			$formdata->class=Input::get('class');
			$formdata->section=Input::get('section');
			$formdata->shift=Input::get('shift');
			$formdata->session=Input::get('session');
			$formdata->subject=Input::get('subject');
			$formdata->exam=Input::get('exam');

			if(count($marks)==0)
			{
				$noResult = array("noresult"=>"No Results Found!!");
				//return Redirect::to('/mark/list')->with("noresult","No Results Found!!");
				return View::Make('app.markList',compact('classes2','subjects','marks','noResult','formdata'));
			}
			$subjects = Subject::all();

			return View::Make('app.markList',compact('classes2','subjects','marks','formdata'));
		}
	}

	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return Response
	*/
	public function edit($id)
	{
        $marks=	DB::table('Marks')
		->join('Student', 'Marks.regiNo', '=', 'Student.regiNo')
		->select('Marks.id','Marks.regiNo','Student.rollNo', 'Student.firstName','Student.middleName','Student.lastName','Marks.subject','Marks.class', 'Marks.written','Marks.mcq','Marks.practical','Marks.ca','Marks.total','Marks.grade','Marks.point','Marks.Absent')
		->where('Marks.id','=',$id)
		->first();
        $subject = Subject::where('code',$marks->subject)->get()->first();
		return View::Make('app.markEdit',compact('marks','subject'));


	}


	/**
	* Update the specified resource in storage.
	*
	* @param  int  $id
	* @return Response
	*/
	public function update()
	{
		$rules=[
		/* 	'written' => 'required',
			'mcq' => 'required',
			'practical' =>'required',
            'ca' =>'required', */
            'total'=>'required',
            'Absent'=>'required',
			'subject' => 'required',
			'class' => 'required'
		];
		$validator = \Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::to('/mark/edit/'.Input::get('id'))->withErrors($validator);
		}
		else {

                $marks = Marks::find(Input::get('id'));
                $marks->total=Input::get('total');
                $subject = Subject::where('code',Input::get('subject'))->get()->first();
                $percentage = ($marks->total*100)/$subject->totalfull;
                if($percentage>=80) $grade = "A";
                if($percentage>=65 and $percentage<80) $grade = "B";
                if($percentage>=50 and $percentage<65) $grade = "C";
                if($percentage>=35 and $percentage<50) $grade = "D";
                if($percentage<35) $grade = "E" ;
                $marks->grade=$grade;
                $marks->point= 0;
                $marks->Absent=Input::get('Absent');
                $marks->save();
                return Redirect::to('/mark/list')->with("success","Marks Update Sucessfully.");

		}
	}
}
