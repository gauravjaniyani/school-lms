
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="img/abc.png">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>

.bg{
 width: 100%;
 background-color:#dcdcdc;
}
.bg2{
 width: 100%;
background-color:#cccccc;
}
.bg3{
  width: 100%;
  font-size: 10px;
  
}

.bg3 tr:nth-child(even) {
    background-color: #ffffff;
}
.bg3 tr:nth-child(odd) {
    background-color: #d8d8d8;
}
.bg3 th {
  height: 25px;
  border: 1px solid #ffffff;
  text-align: center;
  background-color: #cccccc;
  text-transform: uppercase;
  font-size: 9px;
  font-weight: bold;
}
.bg3 td {
  border: 1px solid #ffffff;
}
.t1 {
  width: 40px;
  text-align: center;
}
.t2 {
  width: 50px;
  text-align: center;
}
.t3 {
  width: 40px; 
  text-align: center;
}
.t4 {
  width: 50px;
  text-align: center; 
}
.t5 {
  width: 50px;
  padding-left: 4px; 
}
.t6 {
  width: 270px; 
  padding-left: 4px;
}
.t7 {
 padding-left: 4px; 
}
.t8 {
 padding-left: 5px; 
}
.t9 {
  padding-left: 4px;
}
table {
border-spacing: 0;
border-collapse: separate;

}
table td{
padding-left: 0px;
}
.thead td{
  font-weight: bold;
  color:blue;
}
.red
{
  color:red;
  font-weight: bold;
}
.green {
  color:green;
  font-weight: bold;
}
.logo{
  height: 170px;
  width: 150px;
}
.lefthead{
  width: 20%;
}
.righthead{
  width: 80%;
}
.righthead p{
  margin: 0px;
  padding: 0px;
}
.bg3 tr:last-child {
    background-color: #cccccc;
    height: 25px;
    font-size: 10px;
    font-weight: bold;

}
.bg3 tr:last-child td {
border-top: solid #000 2px;
margin-top:10px !important;
}
#footer
{

width:100%;
height:50px;
position:absolute;
bottom: 0px;
left:0;
}
</style>
</head>

<body >
<div id="admit">
  <table class="bg">
    <tr>
    <td class="lefthead">

     <img class="logo" src="./img/logo.png">
    </td>

   <td class="righthead">
  <!--   <h3>{{$institute->name}}</h3>  -->
      <h2>SHRI LADHARAM SCHOOL (ENGLISH MEDIUM)</h2>
  <pre>
<p><strong>Email:</strong> {{$institute->email}}</p>
<p><strong>Phone:</strong> {{$institute->phoneNo}}</p>
<p><strong>Address:</strong> {{$institute->address}}</p>
     </pre>
   </td>
   </tr>

 </table>
 <table class="bg2">
   <tr>
    <td>

  </td>
  <td>  <strong>Fees Collection Report</strong></td>
  <td>

  </td>
</tr>
</table>
<br>
<center><h2>Collection Time Period</h2></center>
<table style="width: 100%;">
<tr>
   
  <td style="text-align: center;">From Date:  &nbsp;  <strong>   {{$rdata['sDate']}}</strong></td>
  
  <td style="text-align: center;">To Date:   &nbsp;&nbsp;<strong>   {{$rdata['eDate']}}</strong></td>

</tr>
</table>

<center><h2>Fees Collection Report</h2></center>
<table class="bg3">
  <tr>
    <th class="t1">Bill No</th>
    <th class="t2">Pay Date</th>
    <th class="t3">Std.</th>
    <th class="t4">Section</th>
    <th class="t5">Reg No.</th>
    <th class="t6">Name</th>
    <th class="t7">Payable Amount</th>
    <th class="t8">Paid Amount</th>
    <th class="t9">Due Amount</th>
   
  </tr>
  @foreach($list_student as $key => $value)   
   <tr>
    <td class="t1">{{$value->billNo}}</td>
    <td class="t2">{{$value->payDate}}</td>
    <td class="t3">{{$value->class}}</td>
    <td class="t4">{{$value->section}}</td>
    <td class="t5">{{$value->regiNo}}</td>
    <td class="t6">{{$value->lastName}} {{$value->firstName}} {{$value->middleName}}</td>
    <td class="t7">Rs. {{$value->payableAmount}}</td>
    <td class="t8">Rs. {{$value->paidAmount}}</td>
    <td class="t9">Rs. {{$value->dueAmount}}</td>
   
  </tr> 
  @endforeach
   <tr>
      <td class="t1"></td>
      <td class="t2"></td>
      <td class="t3"></td>
      <td class="t4"></td>
      <td class="t5"></td>
      <td class="t6"></td>
      <td class="t7"></td>
      <td class="t8"></td>
      <td class="t9"></td>
      <!-- <td>Payable Amount</td>
      <td>Paid Amount</td>
      <td>Due Amount</td> -->

    </tr>


    <tr>
     
       <td class="t6" colspan="6" style="text-align: right; padding-right: 10px;">
          <strong>TOTAL AMOUNT</strong></td>
       <td class="t7">Rs.  <strong>{{$datas->payTotal}}</strong></td>
       <td class="t8">Rs.  <strong>{{$datas->paiTotal}}</strong>   </td>
       <td class="t9">Rs.  <strong>{{$datas->dueamount}}</strong> </td>

    </tr>
</table>
<br>
<!-- <table id="datat" class="bg3">
  <tbody>
    <tr class="thead">
        <td>Payable Amount</td>
        <td>Paid Amount</td>
        <td>Due</td>

    </tr>


    <tr>

      <td>Rs.  <strong>{{$datas->payTotal}}</strong>
           </td>
      <td>Rs.  <strong>{{$datas->paiTotal}}</strong>   </td>
      <td>Rs.  <strong>{{$datas->dueamount}}</strong> </td>

    </tr>
  </tbody>
</table> -->
<br>
<center>-----0-----</center>
<div id="footer">
  <p>Print Date: {{date('d/m/Y')}}</p>
</div>
</div>
</body>
</html>
