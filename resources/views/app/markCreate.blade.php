@extends('layouts.master')
@section('style')
<link href="/css/bootstrap-datepicker.css" rel="stylesheet">

@stop
@section('content')
@if (Session::get('success'))
<div class="alert alert-success">
  <button data-dismiss="alert" class="close" type="button">×</button>
    <strong>Process Success.</strong> {{ Session::get('success')}}<br><a href="/mark/list">View List</a><br>

</div>
@endif

<form role="form" action="/mark/create" method="post" enctype="multipart/form-data">
<div class="row" ng-controller="marksCtrl">
<div class="box col-md-12">

        <div class="box-inner">
            <div data-original-title="" class="box-header well">
                <h2><i class="glyphicon glyphicon-user"></i> Marks Entry</h2>

            </div>
          <div class="box-content">

            @if (count($errors) > 0)
                                  <div class="alert alert-danger">
                                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                      <ul>
                                          @foreach ($errors->all() as $error)
                                              <li>{{ $error }}</li>
                                          @endforeach
                                      </ul>
                                  </div>
                  @endif

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
                <div class="col-md-12">

                        <div class="col-md-3">
                          <div class="form-group">
                          <label class="control-label" for="class">Class</label>

                          <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-home blue"></i></span>
                              <select id="class" name="class" class="form-control">
                                  <option value="">Select</option>

                                                             <option value="cl1">Std-1</option>
                                                              <option value="cl10">Std-10</option>
                                                              <option value="cl11">Std-11</option>
                                                              <option value="cl12">Std-12</option>
                                                              <option value="cl13">Nursery</option>
                                                              <option value="cl14">Jr.Kg.</option>
                                                              <option value="cl15">Sr.Kg.</option>
                                                              <option value="cl2">Std-2</option>
                                                              <option value="cl3">Std-3</option>
                                                              <option value="cl4">Std-4</option>
                                                              <option value="cl5">Std-5</option>
                                                              <option value="cl6">Std-6</option>
                                                              <option value="cl7">Std-7</option>
                                                              <option value="cl8">Std-8</option>
                                                              <option value="cl9">Std-9</option>

                              </select>
                          </div>
                      </div>
                        </div>
                          <div class="col-md-3">
                            <div class="form-group">
                            <label class="control-label" for="section">Section</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign blue"></i></span>
                                <select id="section" name="section" class="form-control">
                                  <option value="A">A</option>
                                    <option value="B">B</option>
                                      <option value="C">C</option>
                                    <option value="D">D</option>
                                    <option value="E">E</option>
                                      <option value="F">F</option>
                                        <option value="G">G</option>
                                          <option value="H">H</option>
                                              <option value="I">I</option>
                                            <option value="J">J</option>

                               </select>


                            </div>
                          </div>
                            </div>

                            <div class="col-md-3">
                              <div class="form-group">
                              <label class="control-label" for="shift">Shift</label>

                              <div class="input-group">
                                  <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign blue"></i></span>
                                  <select id="shift" name="shift" class="form-control">
                                      <option value="Morning">Morning</option>
                                    <option value="Day">Day</option>

                                 </select>

                              </div>
                            </div>
                              </div>

                              <div class="col-md-3">
                                    <div class="form-group">
                                    <label class="control-label" for="exam">Examination</label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign blue"></i></span>
                                        <select id="exam" name="exam" required="true" class="form-control">
                                                <option value="">Select</option>
                                                <option value="Class Test">Class Test</option>
                                                <option value="Model Test">Model Test</option>
                                                <option value="First Term">First Term</option>
                                                <option value="Mid Term">Mid Term</option>
                                                <option value="Final Exam">Final Exam</option>
                                               
                                        </select>


                                    </div>
                                  </div>
                                    </div>
                              <div class="col-md-3">
                                    <div class="form-group ">
                                                     <label for="session">session</label>
                                                         <div class="input-group">

                                                          <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> </span>
                                                            <input type="text" id="session" required="true" class="form-control datepicker2" name="session" data-date-format="yyyy">
                                                        </div>
                                                 </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                        <label class="control-label" for="subject">subject</label>

                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-book blue"></i></span>
                                            <select id="subject" name="subject" class="form-control" required="true">
                                              <option value="">select</option>
                                              </select>
                                        </div>
                                    </div>
                                      </div>

                                      <div class="col-md-3">
                                            <label class="control-label" style="
                                            margin-top: 35px;
                                        "><b>Total Marks :</b> <span id="tfull"></span></label>
                                        </div>
                                        <div class="col-md-3">
                                                <label class="control-label" style="
                                                margin-top: 35px;
                                            "><b>Pass Marks :</b> <span id="tpass"></span></label>

                                      </div>

                      </div>
        </div>

        <br>
{{--
<div class="row">
        <div class="col-md-12">
                <div class="row">
                  <div class="col-md-2">

                  </div>
                  <div class="col-md-2">
                    <label>Marks</label>
                  </div>
                  <div class="col-md-2">
                     <label>Written</label>
                  </div>
                  <div class="col-md-2">
                   <label>MCQ</label>
                  </div>
                  <div class="col-md-2">
                     <label>Practical</label>
                  </div>
                  <div class="col-md-2">
                     <label>SBA</label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-2">
                  <label>Full Marks</label>
                  </div>
                  <div class="col-md-2">
                    <label id="tfull">0</label>
                  </div>
                  <div class="col-md-2">
                     <label id="wfull">0</label>
                  </div>
                  <div class="col-md-2">
                   <label id="mfull">0</label>
                  </div>
                  <div class="col-md-2">
                     <label id="pfull">0</label>
                  </div>
                  <div class="col-md-2">
                     <label id="cfull">0</label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-2">
                  <label>Pass Marks</label>
                  </div>
                  <div class="col-md-2">
                    <label id="tpass">0</label>
                  </div>
                  <div class="col-md-2">
                     <label id="wpass">0</label>
                  </div>
                  <div class="col-md-2">
                   <label id="mpass">0</label>
                  </div>
                  <div class="col-md-2">
                     <label id="ppass">0</label>
                  </div>
                  <div class="col-md-2">
                     <label id="cpass">0</label>
                  </div>
                </div>
             </div>
</div>
  --}}
     <div class="row">
                           <div class="col-md-12">
                               <div class="table-responsive">
                                   <table id="studentList" class="table table-striped table-bordered table-hover" >

                                       <tr>

                                        <th>Registration No</th>
                                        <th>Roll No</th>
                                        <th>Name</th>
                                        <th>Absent</th>
                                        <th>Marks</th>

                                       </tr>

                                       <tbody>


                                       <tbody>
                               </table>
                           </div>
                       </div>

        </div>

        <!--button save -->
        <div class="row">
         <div class="col-md-12">
           <button class="btn btn-primary pull-right" id="btnsave" type="submit"><i class="glyphicon glyphicon-plus"></i>Save</button>

          </div>
         </div>
       </div>
    </div>
</div>
</div>
</form>
@stop
@section('script')
<script src="/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $( document ).ready(function() {
       $('#btnsave').hide();
        $('#class').on('change', function (e) {
            var val = $(e.target).val();
            $.ajax({
                url:'/class/getsubjects/'+val,
                type:'get',
                dataType: 'json',
                success: function( json ) {
                    $('#subject').empty();
                    $('#subject').append($('<option>').text("Select").attr('value',""));
                    if(false){
                        
                        $.each(json, function(i, subject) {
                            if(subject.type=="Major")
                                $('#subject').append($('<option>').text(subject.name).attr('value', subject.code));
                        });
                        $('#subject').append($('<option disabled>').text("-------------------- Option 1 ----------------------").attr('value',""));
                        $.each(json, function(i, subject) {
                            if(subject.type=="Option 1")
                                $('#subject').append($('<option>').text(subject.name).attr('value', subject.code));
                        });
                        $('#subject').append($('<option disabled>').text("-------------------- Option 2 ----------------------").attr('value',""));
                                $.each(json, function(i, subject) {
                                    if(subject.type=="Option 2")
                                        $('#subject').append($('<option>').text(subject.name).attr('value', subject.code));
                                });
                    }else{
                        $.each(json, function(i, subject) {
                            if(subject.exam_type=="T")
                                $('#subject').append($('<option>').text(subject.name).attr('value', subject.code));
                        });
                        $('#subject').append($('<option disabled>').text("---------------------- Practical -------------------------").attr('value',""));
                        $.each(json, function(i, subject) {
                            if(subject.exam_type=="P")
                                $('#subject').append($('<option>').text(subject.name).attr('value', subject.code));
                        });
                    }
                  
                }
            });
            });

            $(".datepicker2").datepicker( {
    format: " yyyy", // Notice the Extra space at the beginning
    viewMode: "years",
    minViewMode: "years",
    autoclose:true

}).on('changeDate', function (ev) {
    getData();
});

function getData(){

    var aclass = $('#class').val();
    var section =  $('#section').val();
    var shift = $('#shift').val();
    var aclass = $('#class').val();
    var section =  $('#section').val();
    var shift = $('#shift').val();
    var session = $('#session').val().trim();
    if(aclass == "" || section == "" || shift == ""){
        alert("Please, select class, section and shift.")
        return false;
    }

    var subject = $('#subject').val();
    if(subject == ""){
        return false;
    }

    var session = $('#session').val().trim();

    $.ajax({
          url: '/student/getList/'+subject+'/'+aclass+'/'+section+'/'+shift+'/'+session,
          data: {
              format: 'json'
          },
          error: function(error) {

          },
          dataType: 'json',
          success: function(data) {

            $("#studentList").find("tr:gt(0)").remove();
            if(data.length>0)
            {
              $('#btnsave').show();
            }

            for(var i =0;i < data.length;i++)
             {
               addRow(data[i],i);
             }

          },
          type: 'GET'
      });

}
        $( "#subject" ).change(function() {
            getData();
          $.ajax({
                url: '/subject/getmarks/'+$('#subject').val()+'/'+$('#class').val(),
                data: {
                    format: 'json'
                },
                error: function(error) {
                    
                },
                dataType: 'json',
                success: function(data) {
                if(data != undefined && data.length>0){
                    $('#tfull').text(data[0]['totalfull']);
                    $('#tpass').text(data[0]['totalpass']);
                }else{
                    $('#tfull').text("Marks not added");
                    $('#tpass').text("Marks not added");
                    alert("Marks not found for this subject");
                }


                 /* $('#wfull').text(data[0]['wfull']);
                  $('#wpass').text(data[0]['wpass']);

                  $('#mfull').text(data[0]['mfull']);
                  $('#mpass').text(data[0]['mpass']);

                  $('#pfull').text(data[0]['pfull']);
                  $('#ppass').text(data[0]['ppass']);

                  $('#cfull').text(data[0]['sfull']);
                  $('#cpass').text(data[0]['spass']); */
                },
                type: 'GET'
            });

             });



    });
    function addTd(option){
        var td = $("td");
        return td;
    }
    function addRow(data,index) {
    boardTrHtml = "<tr><td><label>1709242</label><input classname='addRegNo[]' type='hidden' value='1709242'></td><td><label>1</label></td><td><label>MOHAMMED AAMIR MOHAMMED RASHID ANSARI</label></td><td><select name='addIsabsent[]' class='absent' style='height: 25px;'><option selected=''>No</option><option>Yes</option></select></td><td><input name='addfa1[1709242]' class='fa1' required='' size='3'></td><td><input name='addfa2[1709242]' class='fa1' required='' size='3'></td><td><input name='addsa1[1709242]' class='fa1' required='' size='3'></td><td><input type='text' data-row='1' name='addMarks[1709242]' class='obtainMarks' required='' size='3'></td></tr>";
    var row ="<tr></tr>";
    $("#studentList>tbody").append(row);


    $examTypeArray = {"Class Test":"class_test","Model Test":"model_test","First Term":"first_term","Mid Term":"mid_term","Final Exam":"final_exam"}
    $selectedExamType = $("#exam").val();
    $examType = $examTypeArray[$selectedExamType];
    $id="";
    if(data[$examType] != undefined){
        $id = data[$examType]['id'];
    }
    var table = document.getElementById('studentList');
    var rowCount = table.rows.length;
    var row = table.insertRow(++index);
    // var cell1 = row.insertCell(0);
    //  var chkbox = document.createElement("label");
    // chkbox.type = "checkbox";
    //chkbox.name="chkbox[]";
    // cell1.appendChild(chkbox);

    var cell2 = row.insertCell(0);
    var regiNo = document.createElement("label");
    regiNo.innerHTML=data['regiNo'];
    cell2.appendChild(regiNo);

    var hdregi = document.createElement("input");

    if(data[$examType] != undefined){
        hdregi.name="editRegNo["+$id+"]";
    }else{
        hdregi.name="addRegNo[]";
    }

    hdregi.value=data['regiNo'];
    hdregi.type="hidden";
    cell2.appendChild(hdregi);

    var cell3 = row.insertCell(1);
    var rollno = document.createElement("label");
    rollno.innerHTML=data['rollNo'];
    cell3.appendChild(rollno);
    var cell4 = row.insertCell(2);
    var name = document.createElement("label");
    name.innerHTML=data['firstName']+' '+data['middleName']+' '+data['lastName'];
    cell4.appendChild(name);
    var absent = row.insertCell(3);
    if(data[$examType] != undefined){
        selectY = data[$examType]['Absent']=='Yes'?'selected':'';
        selectN = data[$examType]['Absent']=='No'?'selected':'';
        absent.innerHTML = "<select name='editIsabsent["+$id+"]' class='absent' style='height: 25px;'><option "+selectN+">No</option><option "+selectY+">Yes</option></select>";
    }else{
        absent.innerHTML = "<select name='addIsabsent[]' class='absent' style='height: 25px;'><option selected>No</option><option>Yes</option></select>";
    }

    $obtainMarksIndex = 4;

    if($selectedExamType == "Final Exam"){
        firstTerm = row.insertCell(4);
        midTerm = row.insertCell(5);
        $total = 0;
        if(data['first_term'] != undefined){
          firstTerm.innerHTML=data['first_term']['total'];
        }
        if(data['mid_term'] != undefined){
            midTerm.innerHTML=data['mid_term']['total'];
        }

       var omColIndex = 6
    }else{
        var omColIndex = 4
        if(false)
        {
            if($id==""){
                rn = data['regiNo'];
                var act = "add";
                omColIndex = 7;
                var txtbox = document.createElement("input");
                txtbox.value = 0;
                txtbox.name = "addfa1["+rn+"]";
                txtbox.className = "fa1";
                txtbox.required = "true";
                txtbox.size="3";
                
                var fa1 = row.insertCell(4);
                fa1.appendChild(txtbox);


                txtbox = document.createElement("input");
                txtbox.value = 0;
                txtbox.name = "addfa2["+rn+"]";
                txtbox.className = "fa1";
                txtbox.required = "true";
                txtbox.size="3";
                var fa2 = row.insertCell(5);
                fa2.appendChild(txtbox);

                txtbox = document.createElement("input");
                txtbox.value = 0;
                txtbox.name = "addsa1["+rn+"]";
                txtbox.className = "fa1";
                txtbox.required = "true";
                txtbox.size="3";
                var sa1 = row.insertCell(6);
                sa1.appendChild(txtbox);
            }else{
                var act = "edit";

                var fa1val = data[$examType]['fa1'];
                var fa2val = data[$examType]['fa2'];
                var sa1val = data[$examType]['sa1'];
                var act = $id==""?"add":'edit';
                omColIndex = 7;
                var txtbox = document.createElement("input");
                txtbox.value = fa1val?fa1val:0;
                txtbox.name = "editfa1["+$id+"]";
                txtbox.className = "fa1";
                txtbox.required = "true";
                txtbox.size="3";
                var fa1 = row.insertCell(4);
                fa1.appendChild(txtbox);


                txtbox = document.createElement("input");
                txtbox.value = fa2val?fa2val:0;
                txtbox.name = "editfa2["+$id+"]";
                txtbox.className = "fa2";
                txtbox.required = "true";
                txtbox.size="3";
                var fa2 = row.insertCell(5);
                fa2.appendChild(txtbox);

                txtbox = document.createElement("input");
                txtbox.value = sa1val?sa1val:0;
                txtbox.name = "editsa1["+$id+"]";
                txtbox.className = "sa1";
                txtbox.required = "true";
                txtbox.size="3";
                var sa1 = row.insertCell(6);
                sa1.appendChild(txtbox);
            }

        }
    }

        var obtainMarks = row.insertCell(omColIndex);
    var om = document.createElement("input");
    om.type="text";

    var attRow = document.createAttribute("data-row");
    attRow.value = index;
    om.setAttributeNode(attRow);

    if(data[$examType] != undefined){
        if(data[$examType]['Absent']=='Yes'){
            var attD = document.createAttribute("disabled");
            attD.value = "disabled";
            om.setAttributeNode(attD);
        }

        om.value = data[$examType]['total'];
        om.name = "editMarks["+$id+"]";
    }else{
        rn = data['regiNo'];
        om.name = "addMarks["+rn+"]";
    }
    om.className = "obtainMarks";
    om.required = "true";
    om.size="3";
    obtainMarks.appendChild(om);
 };
 $("#exam").change(function(){
    var table = document.getElementById('studentList');
    table.innerHTML = "";
    table.insertRow(0);
    table.rows[0].insertCell(0);
    table.rows[0].insertCell(1);
    table.rows[0].insertCell(2);
    table.rows[0].insertCell(3);
    table.rows[0].insertCell(4);
    table.rows[0].cells[0].innerHTML ="Registration No";
    table.rows[0].cells[1].innerHTML ="Roll No";
    table.rows[0].cells[2].innerHTML ="Name";
    table.rows[0].cells[3].innerHTML ="Absent";
    if($(this).val() == "Final Exam"){
        table.rows[0].insertCell(5);
        table.rows[0].insertCell(6);
        table.rows[0].cells[4].innerHTML ="First Term";
        table.rows[0].cells[5].innerHTML ="Mid Term";
        table.rows[0].cells[6].innerHTML ="Annual Marks";
       // table.rows[0].cells[7].innerHTML ="Total Marks";
    }else{
        omColIndex = 4;
        if(false)
        {
            table.rows[0].insertCell(5);
            table.rows[0].insertCell(6);
            table.rows[0].insertCell(7);
            table.rows[0].cells[4].innerHTML ="FA1";
            table.rows[0].cells[5].innerHTML ="FA2";
            table.rows[0].cells[6].innerHTML ="SA1";
            omColIndex = 7;
        }
        table.rows[0].cells[omColIndex].innerHTML ="Marks";
    }
    $("#session").val("");
 });
 $(document).on('focus','input.obtainMarks,input.fa1,input.fa2,input.sa1',function(){
$(this).data('value',$(this).val());
 });
$(document).on('change','input.obtainMarks,input.fa1,input.fa2,input.sa1',function(){
    var className = $(this).attr('class');
    maxVal = 5;
    if(className=='obtainMarks')
    {
        var maxVal = parseInt($("#tfull").html()); 
    }    
    var table = document.getElementById('studentList');
    if($(this).val()>maxVal || $(this).val()<0){
        $(this).val($(this).data('value'));
        $(this).focus();
        alert("Please add marks between 0 and "+maxVal+".");
    }else{
        $(this).data('value',$(this).val());
        row = $(this).data("row");
        $total = 0;
        row = table.rows[row];
    }



});
$(document).on('change','.absent',function(){
    om = $(this).closest("tr").find(".obtainMarks");
    if($(this).val()=='Yes'){

        om.attr("disabled","true");
        om.data("marks",om.val());
        om.val(0);
    }else{
        om.removeAttr("disabled");
        om.val(om.data('marks'));

    }
});
</script>

@stop
