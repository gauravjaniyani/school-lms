@extends('layouts.master')
@section('content')
@if (Session::get('success'))

<div class="alert alert-success">
  <button data-dismiss="alert" class="close" type="button">×</button>
    <strong>Process Success.</strong> {{ Session::get('success')}}<br><a href="/subject/list">View List</a><br>

</div>
@endif
<div class="row">
<div class="box col-md-12">
        <div class="box-inner">
            <div data-original-title="" class="box-header well">
                <h2><i class="glyphicon glyphicon-book"></i> Subject Create</h2>

            </div>
            <div class="box-content">
                                    <div class="row">
                                <div class="col-md-12">
                                  @if (count($errors) > 0)
                                                        <div class="alert alert-danger">
                                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                        @endif
                                    </div>
                                  </div>

                            <form role="form" action="/subject/create" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                      <div class="col-md-12">
                          <h3 class="text-info"> Subject Details</h3>
                          <hr>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-3">
                          <div class="form-group">
                              <label for="name">Code</label>
                              <div class="input-group">
                                  <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign blue"></i></span>
                                  <input type="text" class="form-control" autofocus required name="code" placeholder="Subject Code">
                              </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                              <label for="name">Name</label>
                              <div class="input-group">
                                  <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign blue"></i></span>
                                  <input type="text" class="form-control" required name="name" placeholder="Subject Name">
                              </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                          <label class="control-label" for="type">Type</label>

                          <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign  blue"></i></span>
                              <select id="subject_type" name="type" class="form-control" required>
                              <option value="Core">Core</option>
                              <option value="Option 1">Option 1</option>
                              <option value="Option 2">Option 2</option> 
                              </select>
                          </div>
                      </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                            <label class="control-label" for="type">Exam Type</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign  blue"></i></span>
                                <select id="exam_type" name="exam_type" class="form-control" required>
                                  <option value="T">Theory</option>
                                  <option value="P">Practical</option>
                                    {{--  <option value="B">Board</option>  --}}
                                </select>
                            </div>
                        </div>
                          </div>

                                    </div>
                    </div>



                    <div class="row">
                      <div class="col-md-12">
                          <div class="col-md-3">
                              <div class="form-group">
                                  <label class="control-label" for="stdgroup">Subject Group</label>
                                  <div class="input-group">
                                      <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign  blue"></i></span>
                                      <select name="subgroup" class="form-control" required >
                                          <option value="N/A">N/A</option>
                                          <option value="English">English</option>
                                          <option value="Hindi">Hindi</option>
                                          <option value="Gujarati">Gujarati</option>


                                      </select>
                                  </div>
                              </div>
                          </div>
                    <div class="col-md-3">
                      <div class="form-group">
                            <label class="control-label" for="stdgroup">Student Group</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign  blue"></i></span>
                                <select name="stdgroup" class="form-control" required >
                                  <option value="N/A">N/A</option>
                                  <option value="Science">Science</option>
                                  <option value="Arts">Arts</option>
                                  <option value="Commerce">Commerce</option>

                                </select>
                            </div>
                        </div>
                    </div>
                      <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label" for="class">Class</label>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home blue"></i></span>
                                    <select name="class" class="form-control" required >
                                      @foreach($classes as $class)
                                        <option value="{{$class->code}}">{{$class->name}}</option>
                                      @endforeach

                                    </select>
                                </div>
                            </div>
                      </div>
                      <div class="col-md-3">
                          <div class="form-group">
                        <label for="for">Grade System</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign blue"></i></span>
                            <select id="grade_system" name="gradeSystem" class="form-control" required>
                              <option value="1" selected>100 Marks</option>
                              <option value="2">50 Marks </option>
                            </select>
                        </div>
                    </div>
                      </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                      <div class="col-md-3">
                        <div class="form-group">
                            <label for="totalfull">Marks</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign blue"></i></span>
                                <input type="text" class="form-control" required="true" name="totalfull"  placeholder="0">
                            </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                            <label for="totalpass">Passing Marks</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign blue"></i></span>
                                <input type="text" class="form-control" name="totalpass" required="true"  placeholder="0">
                            </div>
                        </div>
                      </div>
                    </div>
                </div>


                <div class="row">
                <div class="col-md-12">

                    <button class="btn btn-primary pull-right" type="submit"><i class="glyphicon glyphicon-plus"></i>Add</button>

                  </div>
                </div>
                </form>
        </div>
    </div>
</div>
</div>

@stop

@section('script')
    <script src="/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        $(document).on('change','#subject_type',function(){
            var subject_type = $(this).val();
        $("#exam_type").attr("readonly",false);
        $("#grade_system").attr("readonly",false);
            $("#exam_type").val("T");
        });
    </script>
@stop

