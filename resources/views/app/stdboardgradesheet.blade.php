<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>Marks Sheet</title>
      <link rel="stylesheet" type="text/css" href="/markssheetcontent/style.css">
      <link rel="stylesheet" type="text/css" href="/markssheetcontent/result.css">
      <link rel="stylesheet" type="text/css" href="/markssheetcontent/fonts.css">
      <link rel="stylesheet" type="text/css" href="/markssheetcontent/stylesheet.css">
      <script type="text/javascript">
         //<![CDATA[
         var Croogo = {"basePath":"\/","params":{"controller":"student_results","action":"index","named":[]}};
         //]]>
      </script>
      <script type="text/javascript" src="/markssheetcontent/jquery-1.8.2.min.js"></script>
      <script type="text/javascript" src="/markssheetcontent/js.js"></script>
      <script type="text/javascript" src="/markssheetcontent/admin.js"></script>
      <style>
      @page { size: auto;  margin: 0mm; }
      </style>
      <style type="text/css">


         .btn-print {
         color: #ffffff;
         background-color: #49b3e2;
         border-color: #aed0df;
         float: right;
         font-size: 40px;
         padding: 5px;
         margin: 10px 10px;
         border-radius: 20px;
         }
         .btn-print:hover,
         .btn-print:focus,
         .btn-print:active,
         .btn-print.active,
         .open .dropdown-toggle.btn-print {
         color: #ffffff;
         background-color: #0F9966;
         border-color: #19910E;
         }
         .btn-print:active,
         .btn-print.active,
         .open .dropdown-toggle.btn-print {
         background-image: none;
         }
         .btn-print .badge {
         color: #34BA1F;
         background-color: #ffffff;
         }
      </style>
      <link rel="shortcut icon" href="img/abc.png">
   </head>
   <body class="scms-result-print">
      <button class="btn-print" onclick="printDiv('printableArea')">Print</button>
      <div id="printableArea">
         <div class="wraperResult">
            <div class="resHdr">
               <img src="/markssheetcontent/res-logo.png" alt="" class="resLogo">
               <div class="schoolIdentity">
                  <img src="/markssheetcontent/school-title.png" alt="">
                  <div class="hdrText">

                    <!-- <strong>{{$student->class}} {{$student->session}} </strong> -->
                     <span style="line-height: 28px; font-weight: 600;">{{$extra[0]}} EXAMINATION-{{$student->session}}</span>
                       <span>{{$student->class}}  </span>

                  </div>
                  <!-- end of hdrText -->
               </div>
               <!-- end of schoolIdentity -->
            </div>
            <!-- end of resHdr -->
            <div class="resContainer">
               <div class="resTophdr">
                  <div class="restopleft">
                     <div><b>{{$student->firstName}} {{$student->middleName}} {{$student->lastName}}</b></div>
                     <div><span>FATHER'S NAME</span><i>: </i><em>{{$student->fatherName}}</em></div>
                     <div><span>MOTHER'S NAME</span><i>: </i><em>{{$student->motherName}}</em></div>
                     <!-- <div><span>STUDENT ID</span><i>: </i><em>{{$student->regiNo}}</em></div> -->
                     <div><span>DATE OF BIRTH</span><i>: </i><em>{{$student->dob}}</em></div>
                     <div><span>G.R. No.</span><i>: </i><em>{{$student->general_register_number }}</em></div>
                     <!--<div><span>NEW CLASS ROLL :  </span><em>02</em></div>-->
                     <!-- <div><span>SHIFT</span><i>: </i><em>{{$student->shift}}</em></div> -->
                     <!-- <div><span>BOARD</span><i>: </i><em>GSEB</em></div> -->
                  </div>
                  <!-- end of restopleft -->
                  <div class="restopleft rgttopleft">
                     <div><span>CLASS</span><i>: </i><em>{{$student->class}}</em></div>
                     {{--<div><span>GROUP</span><i>: </i><em>{{$student->group}}</em></div>--}}
                     <div><span>SECTION</span><i>: </i><em>{{$student->section}}</em></div>
                     <div><span>ROLL NO</span><i>: </i><em>{{$student->rollNo}}</em></div>
                     {{--
                     <div><span>GPA</span><i>: </i><em>{{$meritdata->point}}</em></div>
                     --}}

                     <!-- <div><span>RANK</span><i>: </i><em>{{ $rank }}</em></div> -->
                     <!--<div><span>PROMOTED CLASS : </span><em>9 (B)</em></div>-->
                  </div>
                  <!-- end of restopleft -->
               </div>
               <!-- end of resTophdr -->
               <div class="resmidcontainer">
                     <h2 class="markTitle">MAJOR SUBJECTS</h2>      
                  <table class="pagetble_middle">
                     <tbody>
                        <tr>
                           <th class="res2 cTitle" ><b>SUBJECT</b></th>
                           @if($student->classcode =='cl10')
                           <th class="res2 cTitle" ><b>F1</b></th>
                           <th class="res2 cTitle" ><b>F2</b></th>
                           <th class="res2 cTitle" ><b>S1</b></th>
                           <th class="res2 cTitle" ><b>Total</b></th>
                           <th class="res2 cTitle" ><b>{{$extra[0]}}</b></th>
                           @endif
                           
                        </tr>
                        @php

                        $totalObtainMarks = 0;
                        $practicalMarksFound = false;
                        $totalMarks = 0;
                        @endphp

                        @foreach($marksList as $marks)
                           @if(isset($marks->subjectInfo) and $marks->subjectInfo->type=='Major')
                           @php
                              $totalSubMarks = $marks->fa1+$marks->fa2+$marks->sa1;
                              $totalObtainMarks += $totalSubMarks;
                              $totalMarks += 15; 
                           @endphp
                           <tr>
                              <td class="res2 cTitle" style=" text-transform: uppercase;">{{ $marks->subjectInfo->name }}</td>
                              <td class="res2 cTitle">{{ $marks->fa1 }} </td>
                              <td class="res2 cTitle">{{ $marks->fa2 }}</td>
                              <td class="res2 cTitle">{{ $marks->sa1 }}</td>
                              <th class="res2 cTitle" >{{ $marks->fa1+$marks->fa2+$marks->sa1 }}</th>
                              <td class="res2 cTitle">{{ $marks->total }}</td>
                           </tr>
                           @else
                              @php $optioalMarksFound = true; @endphp
                           @endif
                        @endforeach
                     </tbody>
                  </table>
                  <br>
                  <table class="pagetble_middle">
                     <tbody>
                        <tr style="border: 1px solid #000;">
                           <td class="res2 cTitle" style="border: none;"><b>Total Marks : </b>{{ $totalObtainMarks }}/{{$totalMarks}}</td>
                           <td class="res2 cTitle" style="border: none;"><b>Percentage : </b>{{ number_format(($totalObtainMarks * 100)/$totalMarks,2) }}%</td>
                           <td class="res2 cTitle" style="border: none;">
                              @php

                              $avgMarks = $totalMarks ?($totalObtainMarks * 100)/$totalMarks : 0;
                              @endphp
                              <b>Final Grade : </b>
                              @if($avgMarks>=80) A @endif
                              @if($avgMarks>=65 and $avgMarks<80) B @endif
                              @if($avgMarks>=50 and $avgMarks<65) C @endif
                              @if($avgMarks>=35 and $avgMarks<50) D @endif
                              @if($avgMarks<35) E @endif
                           </td>
                           <td class="res2 cTitle" style="border: none;"><b>Attendance : {{$student->present_days}}/{{$student->school_days}}</b></td>
                        </tr>
                     </tbody>
                  </table>
                  <br>
               </div>
               <div class="resmidcontainer">
                     <h2 class="markTitle">OPTION 1 SUBJECT</h2>      
                  <table class="pagetble_middle">
                     <tbody>
                        <tr>
                           <th class="res2 cTitle" ><b>SUBJECT</b></th>
                           @if($student->classcode =='cl10')
                           <th class="res2 cTitle" ><b>F1</b></th>
                           <th class="res2 cTitle" ><b>F2</b></th>
                           <th class="res2 cTitle" ><b>S1</b></th>
                           <th class="res2 cTitle" ><b>Total</b></th>
                           <th class="res2 cTitle" ><b>{{$extra[0]}}</b></th>
                           @endif
                           
                        </tr>
                        @php

                        $totalObtainMarks = 0;
                        $practicalMarksFound = false;
                        $totalMarks = 0;
                        @endphp

                        @foreach($marksList as $marks)
                           @if(isset($marks->subjectInfo) and $marks->subjectInfo->type=='Option 1')
                           @php
                              $totalSubMarks = $marks->fa1+$marks->fa2+$marks->sa1;
                              $totalObtainMarks += $totalSubMarks;
                              $totalMarks += 15; 
                           @endphp
                           <tr>
                              <td class="res2 cTitle" style=" text-transform: uppercase;">{{ $marks->subjectInfo->name }}</td>
                              <td class="res2 cTitle">{{ $marks->fa1 }} </td>
                              <td class="res2 cTitle">{{ $marks->fa2 }}</td>
                              <td class="res2 cTitle">{{ $marks->sa1 }}</td>
                              <th class="res2 cTitle" >{{ $marks->fa1+$marks->fa2+$marks->sa1 }}</th>
                              <td class="res2 cTitle">{{ $marks->total }}</td>
                           </tr>
                           @else
                              @php $optioalMarksFound = true; @endphp
                           @endif
                        @endforeach
                     </tbody>
                  </table>
                  <br>
               </div>
               <div class="resmidcontainer">
                     <h2 class="markTitle">OPTION 2 SUBJECT</h2>      
                  <table class="pagetble_middle">
                     <tbody>
                        <tr>
                           <th class="res2 cTitle" ><b>SUBJECT</b></th>
                           @if($student->classcode =='cl10')
                           <th class="res2 cTitle" ><b>F1</b></th>
                           <th class="res2 cTitle" ><b>F2</b></th>
                           <th class="res2 cTitle" ><b>S1</b></th>
                           <th class="res2 cTitle" ><b>Total</b></th>
                           <th class="res2 cTitle" ><b>{{$extra[0]}}</b></th>
                           @endif
                           
                        </tr>
                        @php

                        $totalObtainMarks = 0;
                        $practicalMarksFound = false;
                        $totalMarks = 0;
                        @endphp

                        @foreach($marksList as $marks)
                           @if(isset($marks->subjectInfo) and $marks->subjectInfo->type=='Option 2')
                           @php
                              $totalSubMarks = $marks->fa1+$marks->fa2+$marks->sa1;
                              $totalObtainMarks += $totalSubMarks;
                              $totalMarks += 15; 
                           @endphp
                           <tr>
                              <td class="res2 cTitle" style=" text-transform: uppercase;">{{ $marks->subjectInfo->name }}</td>
                              <td class="res2 cTitle">{{ $marks->fa1 }} </td>
                              <td class="res2 cTitle">{{ $marks->fa2 }}</td>
                              <td class="res2 cTitle">{{ $marks->sa1 }}</td>
                              <th class="res2 cTitle" >{{ $marks->fa1+$marks->fa2+$marks->sa1 }}</th>
                              <td class="res2 cTitle">{{ $marks->total }}</td>
                           </tr>
                           @else
                              @php $optioalMarksFound = true; @endphp
                           @endif
                        @endforeach
                     </tbody>
                  </table>
                  <br>
               </div>
               <!-- end of resmidcontainer -->
            </div>
            <!-- end of resContainer -->
            <div class="signatureWraper">
               <div class="signatureCont">
                  <div class="sign-grdn"><b> (Guardian)</b></div>
                  <div class="sign-clsT"><b> (Class Teacher)</b></div>
                  <div class="sign-head">
                     <!--<img src="/markssheetcontent/head-sign.png" alt="" style="left:23px;bottom:21px">-->                <b> (Principal)</b>
                  </div>
                  <div class="gradeinfo">Grade 80-100:A,65-79:B,50-64:C,35-49:D,0-34:E</div>

               </div>
            </div>

            <!-- end of signatureWraper -->
            <!-- <img src="/markssheetcontent/certificate-bg.png" alt="" class="result-bg"> -->
         </div>

         <!-- end of wraperResult -->
      </div>
      <script>
         function printDiv(divName) {
              var printContents = document.getElementById(divName).innerHTML;
              var originalContents = document.body.innerHTML;

              document.body.innerHTML = printContents;

              window.print();

              document.body.innerHTML = originalContents;

         }

      </script>
   </body>
   <!-- end of fromwrapper-->
</html>
