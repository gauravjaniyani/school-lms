<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>Marks Sheet</title>
      <link rel="stylesheet" type="text/css" href="/markssheetcontent/style.css">
      <link rel="stylesheet" type="text/css" href="/markssheetcontent/result.css">
      <link rel="stylesheet" type="text/css" href="/markssheetcontent/fonts.css">
      <link rel="stylesheet" type="text/css" href="/markssheetcontent/stylesheet.css">
      <script type="text/javascript">
         //<![CDATA[
         var Croogo = {"basePath":"\/","params":{"controller":"student_results","action":"index","named":[]}};
         //]]>
      </script>
      <script type="text/javascript" src="/markssheetcontent/jquery-1.8.2.min.js"></script>
      <script type="text/javascript" src="/markssheetcontent/js.js"></script>
      <script type="text/javascript" src="/markssheetcontent/admin.js"></script>
      <style>
      @page { size: auto;  margin: 0mm; }
      </style>
      <style type="text/css">


         .btn-print {
         color: #ffffff;
         background-color: #49b3e2;
         border-color: #aed0df;
         float: right;
         font-size: 40px;
         padding: 5px;
         margin: 10px 10px;
         border-radius: 20px;
         }
         .btn-print:hover,
         .btn-print:focus,
         .btn-print:active,
         .btn-print.active,
         .open .dropdown-toggle.btn-print {
         color: #ffffff;
         background-color: #0F9966;
         border-color: #19910E;
         }
         .btn-print:active,
         .btn-print.active,
         .open .dropdown-toggle.btn-print {
         background-image: none;
         }
         .btn-print .badge {
         color: #34BA1F;
         background-color: #ffffff;
         }
      </style>
      <link rel="shortcut icon" href="img/abc.png">
   </head>
   <body class="scms-result-print">
      <button class="btn-print" onclick="printDiv('printableArea')">Print</button>
      <div id="printableArea">
         <div class="wraperResult">
            <div class="resHdr">
               <img src="/markssheetcontent/res-logo.png" alt="" class="resLogo">
               <div class="schoolIdentity">
                  <img src="/markssheetcontent/school-title.png" alt="">
                  <div class="hdrText">

                    <!-- <strong>{{$student->class}} {{$student->session}} </strong> -->
                     <span style="line-height: 28px; font-weight: 600;">ANNUAL EXAMINATION (2019-20)</span>
                     <!-- <span style="line-height: 28px; font-weight: 600;">{{$extra[0]}} EXAMINATION-{{$student->session}}</span> -->
                       <span>{{$student->class}}  </span>

                  </div>
                  <!-- end of hdrText -->
               </div>
               <!-- end of schoolIdentity -->
            </div>
            <!-- end of resHdr -->
            <div class="resContainer">
               <div class="resTophdr">
                  <div class="restopleft">
                     <div><b>{{$student->firstName}} {{$student->middleName}} {{$student->lastName}}</b></div>
                     <div><span>FATHER'S NAME</span><i>: </i><em>{{$student->fatherName}}</em></div>
                     <div><span>MOTHER'S NAME</span><i>: </i><em>{{$student->motherName}}</em></div>
                     <!-- <div><span>STUDENT ID</span><i>: </i><em>{{$student->regiNo}}</em></div> -->
                     <div><span>DATE OF BIRTH</span><i>: </i><em>{{$student->dob}}</em></div>
                     <div><span>G.R. No.</span><i>: </i><em>{{$student->general_register_number }}</em></div>
                     <!--<div><span>NEW CLASS ROLL :  </span><em>02</em></div>-->
                     <!-- <div><span>SHIFT</span><i>: </i><em>{{$student->shift}}</em></div> -->
                     <!-- <div><span>BOARD</span><i>: </i><em>GSEB</em></div> -->
                  </div>
                  <!-- end of restopleft -->
                  <div class="restopleft rgttopleft">
                     <div><span>CLASS</span><i>: </i><em>{{$student->class}}</em></div>
{{--                     <div><span>GROUP</span><i>: </i><em>{{$student->group}}</em></div>--}}
                     <div><span>SECTION</span><i>: </i><em>{{$student->section}}</em></div>
                     <div><span>ROLL NO</span><i>: </i><em>{{$student->rollNo}}</em></div>
                     {{--
                     <div><span>GPA</span><i>: </i><em>{{$meritdata->point}}</em></div>
                     --}}

                     <!-- <div><span>RANK</span><i>: </i><em>{{ $rank }}</em></div> -->
                     <!--<div><span>PROMOTED CLASS : </span><em>9 (B)</em></div>-->
                  </div>
                  <!-- end of restopleft -->
               </div>
               <!-- end of resTophdr -->
               <div class="resmidcontainer">
                  <h2 class="markTitle">THEORY MARKS</h2>
                  <table class="pagetble_middle">
                     <tbody>
                        <tr>
                           <th class="res2 cTitle" ><b>SUBJECT</b></th>
                           @if($exam=='Final Exam')
                              <th class="res2 cTitle" ><b>First Term Marks (50)</b></th>
                              <th class="res2 cTitle" ><b>Second Term Marks (50)</b></th>
                              <th class="res2 cTitle" ><b>Annual Exam Marks (100)</b></th>
                              <th class="res2 cTitle" ><b>Marks out of 200</b></th>

                           @else
                              <th class="res2 cTitle" ><b>MARKS</b></th>
                           @endif   

                           <th class="res2 cTitle" ><b>GRADE</b></th>
                        </tr>
                        @php

                        $totalObtainMarks = 0;
                        $practicalMarksFound = false;
                        $totalMarks = 0;
                        $totalGivenGrace = 0;
                        $totalgrace = 15;
                        $studentFailed = false;
                        @endphp

                        @php
                        foreach($marksList as $index=>$marks)
                        {
                           if(isset($marks->subjectInfo) and $marks->subjectInfo->exam_type=='T')
                           {
                           if($marks->total<33  && $marks->total > 19){
                                 $requiredGrace = 33 - $marks->total;
                                 $availableGrace = 15 - $totalGivenGrace;
                                 if($requiredGrace <= $availableGrace){
                                       $graceArr[$index] = $requiredGrace;
                                       $totalGivenGrace += $requiredGrace;
                                 }
                                 else{
                                    $studentFailed = true;

                                 }
                           }
                        }
                        }

                        @endphp

                        @foreach($marksList as $index=>$marks)
                        @if(isset($marks->subjectInfo) and $marks->subjectInfo->exam_type=='T')
                        @php

                        $subjectType = $marks->subjectInfo->type;
                        $coreORselectedOptionalSubject = !((in_array($subjectType,array("Option 1","Option 2"))) && $marks->total==0);
                        if($exam =="Final Exam"){
                           $ft = $marks->firstTerm->total_subject_mark;
                           $mt = $marks->midTerm->total_subject_mark;

                           if($ft==100){
                              $ft = 50;
                           }
                           
                           if($mt==100){
                              $mt = 50;
                           }                        
                           
                           $oft = $marks->firstTerm->total_subject_mark==100?$marks->firstTerm->total/2:$marks->firstTerm->total;
                           $omt = $marks->midTerm->total_subject_mark==100?$marks->midTerm->total/2:$marks->midTerm->total;
                           
                           if($coreORselectedOptionalSubject)
                           {
                              $totalSubMarks = $oft+ $omt + $marks->total;
                              $totalSMarks = $marks->total_subject_mark + $ft+$mt;
                           }
                           $totalMarks += $totalSMarks;
                           $totalObtainMarks += $totalSubMarks;
                        }else{
                           if($coreORselectedOptionalSubject)
                           {
                              $totalMarks += $marks->total_subject_mark;
                              $totalObtainMarks += $marks->total;
                           }
                        }


                        @endphp
                        <tr>
                           <td class="res2 cTitle" style=" text-transform: uppercase;">
                              {{ $marks->subjectInfo->name }}</td>

                           @if($exam=='Final Exam')
                              <td class="res2 cTitle">
                                 {{ $marks->firstTerm->total_subject_mark==100?(($marks->firstTerm->total)/2):$marks->firstTerm->total }} </td>
                              <td class="res2 cTitle">{{ $marks->midTerm->total_subject_mark==100?(($marks->midTerm->total)/2):$marks->midTerm->total }}</td>
                              <td class="res2 cTitle">
                                 {{ $marks->total }}
                                 @if(isset($graceArr[$index]) && $studentFailed == false )
                                <b>   + {{ $graceArr[$index] }}  </b>
                                 @endif
                              </td>

                              <td class="res2 cTitle">{{ $totalSubMarks }}</td>



                              <td class="res2 cTitle"></b>
                                @php
                                $avg = ($totalSubMarks * 100)/$totalSMarks;
                                @endphp
                                 @if($avg>=80) A @endif
                                 @if($avg>=65 and $avg<80) B @endif
                                 @if($avg >= 50 and $avg<65) C @endif
                                 @if($avg>=33 and $avg<50) D @endif
                                 @if($avg<33) E @endif
                              </td>
                           @else
                              
                              
                              <td class="res2 cTitle">{{ $coreORselectedOptionalSubject ? $marks->total:"-" }}</td>
                              <td class="res2 cTitle">{{ $coreORselectedOptionalSubject ? $marks->grade:"-" }}</td>
                           @endif




                        </tr>
                        @else
                        @php $practicalMarksFound = true; @endphp
                        @endif
                        @endforeach
                     </tbody>
                  </table>
                  <br>
                  <table class="pagetble_middle">
                     <tbody>
                        <tr style="border: 1px solid #000;">
                           <td class="res2 cTitle" style="border: none;"><b>Total Marks : </b>{{ $totalObtainMarks }}/{{$totalMarks}}</td>
                           <td class="res2 cTitle" style="border: none;"><b>Percentage : </b>{{ number_format(($totalObtainMarks * 100)/$totalMarks,2) }}%</td>

                           <td class="res2 cTitle" style="border: none;"><b>Grace : </b>{{ $totalGivenGrace }}/15</td>
                           <td class="res2 cTitle" style="border: none;">
                              @php

                              $avgMarks = $totalMarks ?($totalObtainMarks * 100)/$totalMarks : 0;
                              @endphp
                              <b>Final Grade : </b>
                              @if($avgMarks>=80) A @endif
                              @if($avgMarks>=65 and $avgMarks<80) B @endif
                              @if($avgMarks>=50 and $avgMarks<65) C @endif
                              @if($avgMarks>=33 and $avgMarks<50) D @endif
                              @if($avgMarks<33) E @endif
                           </td>
                           <td class="res2 cTitle" style="border: none;"><b>Attendance : {{$student->present_days}}/{{$student->school_days}}</b></td>
                        </tr>
                     </tbody>
                  </table>
                  <br>
                  @if($practicalMarksFound)
                  <h2 class="markTitle">PRACTICAL MARKS</h2>
                  <table class="pagetble_middle">
                     <tbody>
                        <tr>
                           <th class="res2 cTitle" ><b>SUBJECT</b></th>
                           @if($exam == "Final Exam")
                           <th class="res2 cTitle" ><b>FINAL TERM GRADE</b></th>
                           <th class="res2 cTitle" ><b>SECOND TERM GRADE</b></th>
                           <th class="res2 cTitle" ><b>ANNUAL GRADE</b></th>
                           @else
                           <th class="res2 cTitle" ><b>GRADE</b></th>
                           @endif
                        </tr>
                        @foreach($marksList as $marks)
                        @if(isset($marks->subjectInfo) and $marks->subjectInfo->exam_type=='P')
                        <tr>
                           <td class="res2 cTitle">{{ $marks->subjectInfo->name }}</td>
                           @if($exam == "Final Exam")
                           <td class="res2 cTitle">{{ $marks->firstTerm->grade }}</td>
                           <td class="res2 cTitle">{{ $marks->midTerm->grade }}</td>
                           <td class="res2 cTitle">{{ $marks->grade }}</td>
                           @else
                           <td class="res2 cTitle">{{ $marks->grade }}</td>
                           @endif
                        </tr>
                        @endif
                        @endforeach
                     </tbody>
                  </table>
                  @endif
               </div>
               <!-- end of resmidcontainer -->
            </div>
            <!-- end of resContainer -->
            <div style="position: absolute; bottom:189px; width: 100%;">
                <div style="width: 85%; overflow: hidden; position: relative; padding: 0px; margin: 0 auto;">
                  <b style="
">Remark:- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; __________________________________________________________________</b>
                  <br><br><br>
                  <b>Special Notes:- __________________________________________________________________</b>
                </div>
            </div>
            <div class="signatureWraper">
               <div class="signatureCont">
                  {{--<div class="sign-grdn"><b>Signature (Guardian)</b></div>--}}
                  {{--<div class="sign-clsT"><b>Signature (Class Teacher)</b></div>--}}
                  <div class="sign-grdn"><b> (Guardian)</b></div>
                  <div class="sign-clsT"><b> (Class Teacher)</b></div>
                  <div class="sign-head">
                     <!--<img src="/markssheetcontent/head-sign.png" alt="" style="left:23px;bottom:21px">-->                <b>Signature (Principal)</b>
                  </div>
                  <!-- <div class="gradeinfo">Grade 80-100:A,65-79:B,50-64:C,35-49:D,0-34:E</div> -->

               </div>
            </div>

            <!-- end of signatureWraper -->
            <!-- <img src="/markssheetcontent/certificate-bg.png" alt="" class="result-bg"> -->
         </div>

         <!-- end of wraperResult -->
      </div>
      <script>
         function printDiv(divName) {
              var printContents = document.getElementById(divName).innerHTML;
              var originalContents = document.body.innerHTML;

              document.body.innerHTML = printContents;

              window.print();

              document.body.innerHTML = originalContents;

         }

      </script>
   </body>
   <!-- end of fromwrapper-->
</html>
