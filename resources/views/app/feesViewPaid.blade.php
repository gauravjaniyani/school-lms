@extends('layouts.master')

@section('content')
@if (Session::get('success'))
<div class="alert alert-success">
  <button data-dismiss="alert" class="close" type="button">×</button>
    <strong>Process Success.</strong><br>{{ Session::get('success') }}<br>
</div>

@endif
@if (Session::get('error'))
    <div class="alert alert-warning">
        <button data-dismiss="alert" class="close" type="button">×</button>
        <strong> {{ Session::get('error')}}</strong>

    </div>
@endif
<div class="row">
<div class="box col-md-12">
        <div class="box-inner">
            <div data-original-title="" class="box-header well">
                <h2><i class="glyphicon glyphicon-book"></i> Paid List</h2>

            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                <table id="feeList" class="table table-striped table-bordered table-hover">
                                                         <thead>
                                                             <tr>
                                                                <td>Bill Number</td>
                                                                <td>Reg. No.</td>   
                                                                <td>Student Name</td>                                           
                                                                <td>Payable Amount</td>                                           
                                                                <td>Paid Amount</td>                                           
                                                                <td>Due Amount</td>
                                                                <td>Pay Date</td>  
                                                             <td>Action</td>                                         
                                                             </tr>
                                                         </thead>
                                                         <tbody>
                                                           @foreach($billEntries as $index=>$billEntry)
                                                             <tr>
                                                                <td>{{ $billEntry->billNo }}</td>
                                                                <td>{{ $billEntry->regiNo }}</td>   
                                                                <td>@if(isset($billEntry->student)) {{$billEntry->student->firstName}} @endif</td>                                                    
                                                                <td>{{ $billEntry->payableAmount }}</td>   
                                                                <td>{{ $billEntry->paidAmount }}</td>   
                                                                <td>{{ $billEntry->dueAmount }}</td>   
                                                                <td data-sort="{{ strtotime($billEntry->payDate) }}">{{ date('d/m/Y',strtotime($billEntry->payDate)) }}</td>   
                                                                <td> @if($index==0)
                                                                    <a title='Delete' class='btn btn-danger' href='{{url("/fees/delete")}}/{{$billEntry->billNo}}'> <i class="glyphicon glyphicon-trash icon-red"></i></a>
                                                                    @endif
                                                                  </td>
                                                             </tr>
                                                           @endforeach
                                                           </tbody>

                     </table>
                     {{ $billEntries->links() }}
                        </div>
                    </div>
                                <br><br>


        </div>
    </div>
</div>
</div>
@stop
@section('script')

<script type="text/javascript">
    $( document ).ready(function() {
    /* 
        $('#feeList').dataTable( {"order": [[ 5, "desc" ]]});
    */
    });
</script>
@stop
